import cv2
import numpy
import math
import pickle
import sys
import bovw_utils
import os
from sklearn.cluster import KMeans
from bovw_hyperparameters import *

numpy.set_printoptions(threshold=sys.maxsize)

print('Loading model...')
load_data = bovw_utils.load()

cluster = load_data['cluster']
classifier = load_data['classifier']
scaler = load_data['scaler']

print('Loading validation dataset...')
dataset = bovw_utils.get_dataset('test')[:]
print('Extracting images features...')
(features, files_features_count) = bovw_utils.get_features(dataset, FEATURES_LIMIT)
print('A total of {} feature(s) were extracted from {} image(s).'.format(len(features), len(files_features_count)))

print('Fitting features in clustering model...')
features_clusters = cluster.predict(features)

print('Calculating histograms of images features...')
bovw = bovw_utils.make_bag_of_visual_words(features_clusters, files_features_count, NUMBER_OF_CLUSTERS)

for index, histogram in enumerate(bovw):
    dataset[index]['histogram'] = histogram

print('Feeding input to classifier model...')
test_x = scaler.transform([ x['histogram'] for x in dataset ])
test_y = classifier.predict(test_x)
test_y_pct = classifier.predict_proba(test_x)

for index, labels in enumerate(test_y):
    correct_labels = [ bovw_utils.CLASSES[x] for x in dataset[index]['labels'] ]
    predicted_labels = [ bovw_utils.CLASSES[i] if x >= 0.4 else None for i, x in enumerate(labels) ]
    predicted_labels = list(filter(lambda x: x is not None, predicted_labels))
    print('{}: predicted = {}, correct = {}'.format(
        dataset[index]['name'], predicted_labels, correct_labels
    ))

dir_path = os.path.dirname(os.path.realpath(__file__))

for label_index in range(0, len(bovw_utils.CLASSES)):
    path = '../results/VOC2007/Main/comp1_cls_test_{}.txt'.format(bovw_utils.CLASSES[label_index])
    path = os.path.join(dir_path, path)
    with open(path, 'w') as file:
        lines = [
            '{} {:.6f}'.format(
                image_data['name'],
                test_y_pct[image_index][label_index]
            ) for image_index, image_data in enumerate(dataset)
        ]
        file.write('\n'.join(lines))
