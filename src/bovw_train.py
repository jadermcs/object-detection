import cv2
import numpy
import math
import pickle
import bovw_utils
import sys
from bovw_hyperparameters import *
from sklearn.cluster import KMeans
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import MultiLabelBinarizer, StandardScaler

numpy.set_printoptions(threshold=sys.maxsize)

def flatten(l):
    return [ i for sl in l for i in sl ]

if __name__ == '__main__':

    load_data = bovw_utils.load()

    print('Loading training dataset...')
    dataset = bovw_utils.get_dataset('trainval')[:]

    if load_data is not None and 'features' in load_data and 'files_features_count' in load_data:
        print('Using previously extracted features...')
        features = load_data['features']
        files_features_count = load_data['files_features_count']
        print('A total of {} feature(s) of {} image(s) were loaded.'.format(len(features), len(files_features_count)))
    else:
        print('Extracting images features...')
        (features, files_features_count) = bovw_utils.get_features(dataset, FEATURES_LIMIT)
        print('A total of {} feature(s) were extracted from {} image(s).'.format(len(features), len(files_features_count)))

    print('Training data labels histogram:')
    histogram_input = flatten([ x['labels'] for x in dataset ])
    dataset_labels_histogram = numpy.histogram(histogram_input, len(bovw_utils.CLASSES))[0]
    for index, value in enumerate(dataset_labels_histogram):
        pct = math.floor(value / len(dataset) * 100)
        print('{}: {} ({}%)'.format(bovw_utils.CLASSES[index], value, pct))

    number_of_features = len(features)
    number_of_samples = len(dataset)
    number_of_clusters = NUMBER_OF_CLUSTERS

    if load_data is not None and 'cluster' in load_data:
        print('Loading previously clustered data...')
        cluster = load_data['cluster']
    else:
        print('Clustering extracted features in {} cluster(s)...'.format(number_of_clusters))
        cluster = KMeans(n_clusters=number_of_clusters, n_jobs=2, verbose=1)
        cluster.fit(features)
    features_clusters = cluster.predict(features)

    print('Creating bag of visual words...')
    bovw = bovw_utils.make_bag_of_visual_words(features_clusters, files_features_count, number_of_clusters)
    for index, histogram in enumerate(bovw):
        if index >= len(dataset):
            break
        dataset[index]['histogram'] = histogram

    print('Training classifier model...')
    scaler = StandardScaler()
    train_x = [ x['histogram'] for x in dataset ]
    train_y = [ x['labels'] for x in dataset ]

    scaler.fit(train_x)
    train_x = scaler.transform(train_x)
    train_y = MultiLabelBinarizer().fit_transform(train_y)

    classifier = OneVsRestClassifier(SVC(probability=True))
    classifier.fit(train_x, train_y)
    bovw_utils.save({
        'classifier': classifier,
        'scaler': scaler,
        'cluster': cluster,
        'features': features,
        'files_features_count': files_features_count
    })

    # _, ax = pyplot.subplots(2)
    # ax[0].scatter(features[:,0], features[:,1])
    # ax[0].set_title("Initial Scatter Distribution")
    # ax[1].scatter(features[:,0], features[:,1], c=k_means)
    # ax[1].set_title("Colored Partition denoting Clusters")
    # pyplot.show()
