import cv2
import numpy
import math
import pickle
import re
from bovw_hyperparameters import *
from matplotlib import pyplot
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs

FILE_PATH_SAVE_DATA = 'train_data.bin'
CLASSES = [
    'aeroplane',
    'bicycle',
    'bird',
    'boat',
    'bottle',
    'bus',
    'car',
    'cat',
    'chair',
    'cow',
    'diningtable',
    'dog',
    'horse',
    'motorbike',
    'person',
    'pottedplant',
    'sheep',
    'sofa',
    'train',
    'tvmonitor'
]

def get_dataset(set = 'train'):
    train_path = '../VOCdevkit/VOC2007/ImageSets/Main/{}.txt'.format(set)
    image_path_fmt = '../VOCdevkit/VOC2007/JPEGImages/{}.jpg'
    labels_path_fmt = '../VOCdevkit/VOC2007/ImageSets/Main/{}_{}.txt'
    dataset = []
    indirection = {}

    with open(train_path, 'r') as file:
        file_names_list = file.read().strip().split('\n')
        for file_name in file_names_list:
            file_data = {
                'name': file_name,
                'path': image_path_fmt.format(file_name),
                'labels': []
            }
            dataset.append(file_data)
            indirection[file_name] = file_data

    re_splitter = re.compile(r'\s+')

    for label_index, label in enumerate(CLASSES):
        label_path = labels_path_fmt.format(label, set)
        with open(label_path, 'r') as file:
            label_values = file.read().strip().split('\n')
            index = 0
            for label_value in label_values:
                (file_name, has_label) = re_splitter.split(label_value.strip())
                if int(has_label) >= 1:
                    indirection[file_name]['labels'].append(label_index)

    return dataset

def get_features(dataset, feature_limit=None):
    freak = cv2.xfeatures2d.FREAK_create()
    fast = cv2.FastFeatureDetector_create()
    samples = None
    files_features_count = []
    for file_data in dataset:
        file_path = file_data['path']
        image = cv2.imread(file_path)
        grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        keypoints = fast.detect(grayscale, None)
        (keypoints, descriptors) = freak.compute(grayscale, keypoints)
        if feature_limit is not None:
            descriptors = descriptors[:feature_limit]
        files_features_count.append(descriptors.shape[0])
        if samples is None:
            samples = descriptors
        else:
            samples = numpy.vstack((samples, descriptors))
    return (samples, files_features_count)

def create_histogram(samples, histogram_length):
    return numpy.histogram(samples, histogram_length, range=(0, NUMBER_OF_CLUSTERS))[0]

def make_bag_of_visual_words(features_clusters, images_features_counts, number_of_clusters):
    bovw = []
    index_begin = 0
    for feature_count in images_features_counts:
        image_features_clusters = features_clusters[index_begin:index_begin + feature_count]
        image_histogram = create_histogram(image_features_clusters, number_of_clusters)
        bovw.append(image_histogram)
        index_begin += feature_count
    return bovw

def save(data):
    with open(FILE_PATH_SAVE_DATA, 'wb') as file:
        pickle.dump(data, file)

def load():
    try:
        with open(FILE_PATH_SAVE_DATA, 'rb') as file:
            return pickle.load(file)
    except:
        return None
