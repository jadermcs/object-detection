\documentclass{bmvc2k}

%% Enter your paper number here for the review copy
% \bmvcreviewcopy{??}

% \usepackage[brazilian]{babel}
\usepackage[utf8]{inputenc}

\title{Multilabel Classification with BOVW and Resnets}

% Enter the paper's authors in order
% \addauthor{Name}{email/homepage}{INSTITUTION_CODE}
\addauthor{Bruno Osorio}{osorio.bruno@hotmail.com}{1}
\addauthor{Jader Martins}{jader.ml}{1}

% Enter the institutions
% \addinstitution{Name\\Address}
\addinstitution{
  Departamento de Ci\^encia da Comptuta\c{c}\~ao\\
  Universidade de Bras\'{\i}lia\\
  Campus Darcy Ribeiro, Asa Norte\\
  Bras\'{\i}lia-DF, CEP 70910-900, Brazil,  
}

\runninghead{Bruno and Jader}{Computer Vision Assignment -- \today}

% Any macro definitions you would like to include
% These are not defined in the style file, because they don't begin
% with \bmva, so they might conflict with the user's own macros.
% The \bmvaOneDot macro adds a full stop unless there is one in the
% text already.
\def\eg{\emph{e.g}\bmvaOneDot}
\def\Eg{\emph{E.g}\bmvaOneDot}
\def\etal{\emph{et al}\bmvaOneDot}

%-------------------------------------------------------------------------
% Document starts here
\begin{document}
\maketitle

\footnote{Both authors contributed to the writing of this document. Bruno is the author of the bag of visual words implementation while Jader is the author of the neural network implementation.}

\begin{abstract}
Image classification consists of inferring what entities are present in an image and then designate a class for it. As one image can contain various relevant entities, it is also possible to give multiple classes to a single image, turning the problem into a more difficult one, because target distribution becomes harder to model. In this paper, we apply techniques for predicting VOC2007 challenge in multilabel problem.
\end{abstract}

%-------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}
Visual Object Classes, known as VOC, is a challenge supported by the EU-funded PASCAL Network of Excellence on Pattern Analysis, Statistical Modelling and Computational Learning. The goal of this challenge is to recognize objects from a number of visual object classes in realistic scenes (i.e. not pre-segmented objects). It is fundamentally a supervised learning problem with a provided training set containing labelled images. The twenty object classes that have been selected are: person, bird, cat, cow, dog, horse, sheep, aeroplane, bicycle, boat, bus, car, motorbike, train, bottle, chair, dining table, potted plant, sofa and tv/monitor. For this work, we address to the image classification one. 

\begin{figure}[h]
    \centering
    \includegraphics[width=100px]{Figs/diningtable_05.jpg}
    \caption{Example image containing in VOC.}
    \label{fig:voc}
\end{figure}


In image processing, we call filter or kernel an operator designed for blurring, sharpening, embossing or detecting edges in images. These operations give high-level comprehensions of images, allowing algorithms to understand complex structures in a given image. There is no uniquely best filter for all tasks, so finding the right filter for a specific problem manually is practically impossible given the infinite possibilities.

The actual state-of-the-art technique consists of finding those filters, exemplified in figure \ref{fig:conv_filter}, through supervised learning, for feature extraction and then applying a multi-layer perceptron over those filters to infer which class(es) an image belongs to. This "learning" procedure is done by minimizing the model error for a given dataset based on some metric, knowing when the mathematical model predicts the result correctly or not during the task, and then with a specialized model to infer the classes for unknown images.

\begin{figure}
    \centering
    \includegraphics[width=100px]{Figs/conv.jpeg}
    \caption{Complex convolution filters learned by a CNN.}
    \label{fig:conv_filter}
\end{figure}

\section{Methodology}
\label{sec:method}
In mathematics, convolution is an operation that measures the amount of overlap between functions $f$ and $g$, as shown in figure \ref{fig:conv_op}. It therefore "blends" one function with another, allowing exploiting properties of $g$ that are interesting over $f$, ex. smoothing, denoising or amplifying $f$. The convolution of $f$ and $g$ is written as $f * g$, using an asterisk or star. It is defined as the integral of the product of the two functions after one is reversed and shifted. It is expressed as:

\begin{equation}
    (f * g)(t) = \int_{-\infty}^\infty f(\tau) g(t - \tau) \, d\tau
\end{equation}

when applying in images we consider the discrete form given by:

\begin{equation}
    (f * g)[n] = \sum_{m=-\infty}^\infty f[n-m] g[m]
\end{equation}

As $g$ in the discrete form is a matrix $NxN$, we call it a kernel, this filter can be learned with back-propagation as introduced by Lecun \etal \cite{lecun1998gradient}, called convolution neural network, (although it is not exactly an convolution, for simplification we will say it is), allowing high quality task-specific filters being discovered, then we apply batch-normalization and residual-blocks, to this architecture for training deeper networks.

\begin{figure}[h]
    \centering
    \includegraphics[width=150px]{Figs/Comparison_convolution_correlation.png}
    \caption{Convolution functional for functions $f$ and $g$.}
    \label{fig:conv_op}
\end{figure}

When training deep networks, we usually get overflow or underflow in the feed-forward step. As stated by the original article\cite{ioffe2015batch}, internal covariate shift, normalizing each batch, called ``batch normalization'', helps reducing those problems, making it possible to train deep networks much faster. Considering the batch B with samples $x_i = (x_i^{(1)},...,x_i^{(d)})$, we have
$\mu_B = \frac 1 m \sum_{i=1}^m x_i$ and $\sigma_B^2 = \frac 1 m \sum_{i=1}^m (x_i-\mu_B)^2$.

And the batch normalization is given by:
\begin{equation}
    \hat{x}_{i}^{(k)} = \frac {x_i^{(k)}-\mu_B^{(k)}}{\sqrt{\sigma_B^{(k)^2}+\epsilon}}
\end{equation}
where $k \in [1,d]$ and  $i \in [1,m]$; $\mu_B^{(k)}$ and $\sigma_B^{(k)^2}$ are the per-dimension mean and variance, respectively.

Residual Networks apply a boosting learning scheme, as shown in \ref{fig:resnet}, we have a mapping $\mathcal{F}$ over $x$ where an $\mathcal{H}(x)$ should be learned to fix the residual error for $\mathcal{F}(x) = \mathcal{H}(x) - x$, each blocking learning a easier $\mathcal{H}$, compared to $\mathcal{F}(x)$, from previous blocks allows the training of extremely deep networks (101 layers)\cite{he2016deep}, obtaining the state-of-the-art for image classification and a good initial hypothesis for other computer vision tasks, through transfer learning\cite{yosinski2014transferable}.

\begin{figure}[h]
    \centering
    \includegraphics[width=150px]{Figs/res.png}
    \caption{Residual block, learns an $\mathcal{H}(x)$ to fix the residual error from previous block.}
    \label{fig:resnet}
\end{figure}

For a multilabel problem, as we have a 20-class problem the last layer of our resnet has a linear layer with input features of size 512 and output features of size 20, we have to optimize an proper loss function, here we chose the binary cross entropy with logits defined as:
\begin{equation}
    \ell(\hat{y}, y) = L = \{l_1,\dots,l_N\}^\top, \quad
    \ell_c(x, y) = L_c = \{l_{1,c},\dots,l_{N,c}\}^\top
\end{equation}
\begin{equation}
    l_{n,c} = - w_{n,c} \left[ p_c y_{n,c} \cdot \log \sigma(x_{n,c})
    + (1 - y_{n,c}) \cdot \log (1 - \sigma(x_{n,c})) \right]
\end{equation}
where $c$ is the class number ($c > 1$ for multi-label binary classification), $n$ is the number of the sample in the batch and $p_c$ is the weight of the positive answer for the class $c$.

Bag of visual words is another technique used to recognize entities in images and assigning classes to it. It is based on the fact that to recognize an entity, e.g. a person, one could look for smaller entities that combined make up the entity that is being looked for, e.g. a person's eyes, mouth, nose etc. It consists of detecting those smaller features in the image, and when they are present, they indicate a higher probability of the main feature being present as well.

To detect smaller features in images, multiple algorithms have been developed to extract unique details in images, for example: SURF\cite{Lowe:2004:DIF:993451.996342}, SIFT\cite{Bay:2008:SRF:1370312.1370556}, FAST\cite{Rosten:2006:MLH:2094437.2094478} etc. Those features detector, putting it simple, look for points in the image in which there is a considerable amount of diversity between the pixel and its neighbours. These comparisons are ways to detect edges, borders, details changes and other image aspects that can uniquely describe what that point in. These relevant points, alongside its neighbourhood area and some other information, make up what is called a keypoint, i.e. a number representation of a small piece of image that is somewhat unique.

Once a large amount of visually common keypoints are extracted, they can be grouped together based on how similar they are. If we need to analyze a keypoint never seen before, it's possible to know if it resembles a given feature if this keypoint can also be grouped with a group of similar keypoints, meaning that this new keypoint resembles the feature represented by the keypoints group and has a high chance of being the same entity.

To detect an animal in a picture, one can extract the keypoints of the animal image, match them against various groups of keypoints describing features and see if enough features that actually belong to an animal are present in the picture (for instance: fur, paws, tail etc).

To group keypoints together, a clustering algorithm is required. It is a mathematical approach that partitions a set of numbers in various groups, dictating in which group a number belongs to based on the distance of that number and the centroid (mean value) of the numbers belonging to that group. By minimizing all distances, it's possible to tell which number are close to one another by following a distance function.

After extracting the keypoints of an image and checking in which cluster the feature represented by the keypoints belong to, one can simply calculate a histogram of the features to see how many times each feature is present. By amassing a dataset of histogram of features and which entities are present in the image of that histogram, it's possible to feed this dataset to a machine learning algorithm, like neural networks presented above, to be able to predict how each count of features impact in the classification of images.


\section{Results}
\label{sec:result}

The bag of visual words technique and convolution neural network was trained in trainval datasets and then measured to the test dataset of the VOC challenge. To verify the precision of the classes prediction, the mean average precision metric was applied to the classification output. Table \ref{table:avg-precision} shows the precision of this technique, also shows the mean of all average precisions.


\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|l|}
\hline
Class       & Average Precision in CNN & Average Precision in BOVW \\
\hline
aeroplane   & 97,1\%  & 19,4\%            \\
bicycle     & 90,9\%  & 7,6\%             \\
bird        & 91,8\%  & 8,3\%             \\
boat        & 89,1\% & 5,6\%             \\
bottle      & 63,0\%  & 5,7\%             \\
bus         & 84,9\% & 4,5\%             \\
car         & 90,6\%  & 19,0\%            \\
cat         & 91,4\%  & 16,2\%            \\
chair       & 73,3\% & 10,4\%            \\
cow         & 85,1\%  & 3,9\%             \\
diningtable & 82,8\%  & 5,2\%             \\
dog         & 90,2\%  & 9,0\%             \\
horse       & 90,2\% & 9,4\%             \\
motorbike   & 89,7\%  & 14,1\%            \\
person      & 94,2\%   & 47,8\%            \\
pottedplant & 70,2\%   & 5,3\%             \\
sheep       & 86,3\%  & 2,7\%             \\
sofa        & 76,5\%  & 8,0\%             \\
train       & 91,8\% & 18,2\%            \\
tvmonitor   & 82,8\%   & 5,2\%             \\
\hline
mean        & 85,6\%  & 11,3\% \\
\hline
\end{tabular}
\end{center}
\caption{Average precision for each class in the bag of visual words model and convolution neural network, last row contains the mean for all classes.}
\label{table:avg-precision}
\end{table}

\section{Conclusion}
\label{sec:conclusion}
As shown by results Convolutional Neural Networks has a great capacity for image recognition, compared to a classical method the CNN presented a huge improvement.



\bibliography{refs}
\end{document}